<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JLoader::register('xmlrpc_client', __DIR__ . '/../vendor/phpxmlrpc/phpxmlrpc/lib/xmlrpc.inc');
JLoader::register('xmlrpcmsg', __DIR__ . '/../vendor/phpxmlrpc/phpxmlrpc/lib/xmlrpc.inc');
JLoader::register('xmlrpcval', __DIR__ . '/../vendor/phpxmlrpc/phpxmlrpc/lib/xmlrpc.inc');
JLoader::register('JSFactory', JPATH_ROOT . '/components/com_jshopping/lib/factory.php');

// import Joomla modelitem library
jimport('joomla.application.component.modelitem');

/**
 * Model for fetchig data from WuBook server by its API
 */
class discountcardsModelWudata extends JModelLegacy
{

    /**
     * Клиент доступа к серверу WuBook
     *
     * @var xmlrpc_client
     */
    private $rpc_client;

    /**
     * Токен для доступа к функциям Wired API
     *
     * @var string
     */
    private $wu_token;

    /**
     * Код гостинницы (lcode)
     *
     * @var string
     */
    private $wu_lcode;

    /**
     * Получает у WuBook токен для доступа к Wired API
     *
     * @throws Exception
     */
    private function acquire_token()
    {
        // Если токен уже есть проверим его валидность
        if (isset($this->wu_token)) {
            $args = array(
                new xmlrpcval($this->wu_token, 'string')
            );
            $message = new xmlrpcmsg('is_token_valid', $args);
            $result = $this->rpc_client->send($message);
            $result = $this->parseRpcValue($result);
            // Если уже имеем токен и он валидный просто выходим
            if (($result[0] == 0) && $result[1]) {
                return;
            }
        }
        // Получим новый токен
        $user = $this->params->get('wu_user', '');
        $pass = $this->params->get('wu_pass', '');
        $pkey = $this->params->get('wu_pkey', '');
        
        $args = array(
            new xmlrpcval($user, 'string'),
            new xmlrpcval($pass, 'string'),
            new xmlrpcval($pkey, 'string')
        );
        
        $message = new xmlrpcmsg('acquire_token', $args);
        $result = $this->rpc_client->send($message);
        $result = $this->parseRpcValue($result);
        // Если токен получен запомним его
        if ($result[0] == 0) {
            $this->wu_token = $result[1];
        } else {
            // Без токена все равно ничего сделать не можем - выбрасваем ошибку
            $this->wu_token = Null;
            throw new Exception(JText::_('COM_DISCOUNTCARDS_ACQURE_TOKEN_ERROR'));
        }
    }

    /**
     * Освобождает используемый токен
     */
    private function release_token()
    {
        if (! isset($this->wu_token)) {
            return;
        }
        $args = array(
            new xmlrpcval($this->wu_token, 'string')
        );
        $message = new xmlrpcmsg('release_token', $args);
        $this->rpc_client->send($message)->value();
        $this->wu_token = Null;
    }

    /**
     * Преобразует значение PhpXmlRpc\Value к простой структуре вложенных массивов все значения, которых скалярны
     *
     * @param PhpXmlRpc\Response $result
     *            - Исходное значение
     * @return mixed - Распарсенное значение
     */
    private function parseRpcValue($result)
    {
        $iterations = 0; // Счетчик итераций по парсингу вложенных значений
        $max_itearions = 1000; // Ограничение максисмального количества итераций, что бы избежать зависаний
        $stack = array(); // Временное хранилище распарсенных значений, что бы избежать рекурсии
        $item_keys = array(); // Хранилище ключей вложенных массивов на случай, если они ассоциативные
        $start_ind = 0; // Индекс, с которого начинается обработка текущего массива (в случае, если обработка была прервана)
        $need_to_save = false; // Флаг того, что текущий массив был обработан полностью и необходимо запомнить его значение
        $current_item = array(
            $result->value()
        ); // Обработка происходит на основе перебора элемнтов массива
        
        while ($iterations < $max_itearions) {
            // Если текущая составляющиая результата запроса является массивом, и не требуется его сохранение
            if (is_array($current_item) && (! $need_to_save)) {
                // Если обработка массива еще не закончена - распарсим при необходимости его значения
                if ($start_ind < count($current_item)) {
                    // Возможно массив ассоциативный. Проиндексируем его значения, что бы иметь возможность прерывав перебор массива, запоминать индекс элемента,
                    // приняться за перебор другого массива, а затеме возврнуься к предыдущему и продолжать перебор с того места, на котором прервались
                    if (count($item_keys) == 0) {
                        $item_keys = array_keys($current_item);
                    }
                    for ($key_ind = $start_ind; $key_ind < count($item_keys); $key_ind ++) {
                        $nested_item = $current_item[$item_keys[$key_ind]];
                        // Если текущее значение массива - PhpXmlRpc\Value, - распарсим его
                        if ($nested_item instanceof PhpXmlRpc\Value) {
                            $nested_item = $nested_item->scalarval();
                            $current_item[$item_keys[$key_ind]] = $nested_item;
                            // Запомним частично обработанный массив предыдущего уровня
                            array_push($stack, array(
                                'index' => $key_ind,
                                'keys' => $item_keys,
                                'processing_item' => $current_item
                            ));
                            // Сбросим значения индексов, что бы он не влияли на перебор вложенного массива
                            $item_keys = array();
                            $start_ind = 0;
                            // Выберем в качестве текущего элемента вложенное значение
                            $current_item = $nested_item;
                            // Прервем пока обработку массива текущего уровня, чтобы прежде обработать вложенный массив
                            break;
                        } elseif (! ($key_ind < (count($item_keys) - 1))) {
                            // Если элемент последний и он скалярный просто сохраним его
                            $need_to_save = true;
                        }
                    }
                } else {
                    // Вернулись к охватывающему массиву, но он весь обработан
                    $need_to_save = true;
                }
            } else {
                // Если больше нет элементов для обработки выходим из цикла
                if (count($stack) > 0) {
                    $item_data = array_pop($stack);
                    // Если текущий элемнт - массив, и он полностью обработан, присвоим его соответствующему элементу охватывющего
                    if ($need_to_save) {
                        $current_key = $item_data['keys'][$item_data['index']];
                        $item_data['processing_item'][$current_key] = $current_item;
                        $need_to_save = false;
                    }
                    // Восстановим данные о массиве из стека
                    $current_item = $item_data['processing_item'];
                    $item_keys = $item_data['keys'];
                    $start_ind = $item_data['index'] + 1;
                } else {
                    // Если стэк пуст вернем исходное значение
                    $result = $current_item[0];
                    break;
                }
            }
            // Считаем итерации, что бы не зациклиться
            $iterations ++;
        }
        
        return $result;
    }

    /**
     * Выполняет запрос к WuBook API, вставляя в по-умолчанию параметры token и lcode
     *
     * @param string $func_name
     *            - Имя функции API
     * @param array $args
     *            - Список аргументов
     * @param bool $caching
     *            - Кешировать запрос
     * @return NULL|\PhpXmlRpc\Value - Результат выполнения запроса
     */
    private function callWuFunc($func_name, $args = array(), $caching = false)
    {
        // Проверим кэш
        if ($caching) {
            $cache = JFactory::getCache('com_discountcards', '');
            $cache_id = $func_name;
            
            foreach ($args as $argument) {
                $cache_id .= '_' . $argument->value;
            }
            
            if ($result = $cache->get($cache_id)) {
                return $result;
            }
        }
        // Получим токен для работы с WuBook Wired API
        try {
            $this->acquire_token();
        } catch (Exception $e) {
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return Null;
        }
        // Добавим обязательные данные для авторизации
        $realy_args = array(
            new xmlrpcval($this->wu_token, 'string'),
            new xmlrpcval($this->wu_lcode, 'int')
        );
        foreach ($args as $name => $argument) {
            // В случае, если в массиве аргументов имеются значения которые мы уже задали, сбросим их
            switch ($name) {
                case 'token':
                    unset($realy_args[0]);
                    break;
                
                case 'lcode':
                    unset($realy_args[1]);
                    break;
            }
            $realy_args[] = new xmlrpcval($argument->value, $argument->type);
        }
        // Выполним запрос
        $message = new xmlrpcmsg($func_name, $realy_args);
        $result = $this->rpc_client->send($message);
        $result = $this->parseRpcValue($result);
        // сохраняем сохраним результат в кэше
        if ($caching) {
            $cache->store($result, $cache_id);
        }
        
        return $result;
    }

    /**
     * Возвращает id-комнаты в сервисе WuBook на основе id-номера и его настроек в JoomShopping
     *
     * @param int $site_room_id
     *            ид номера в JoomShopping
     * @return int ид номера в WuBook
     */
    private function getWuRoomId($site_room_id)
    {
        // Получим код атрибута с типом номера из настроек компонента
        $attr_code = $this->params->get('jsh_attr_id', Null);
        
        if (empty($attr_code)) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_DISCOUNTCARDS_JSHATTR_NOT_SET'), 'error');
            return Null;
        }
        // Получим номер из JoomShopping
        $product = JSFactory::getTable('product', 'jshop');
        $product->load($site_room_id);
        $attributesDatas = $product->getAttributesDatas();
        
        if (! isset($attributesDatas['attributeValues'][$attr_code])) {
            // Атрибут не установлен
            return Null;
        }
        if (preg_match('/^\w+/', $attributesDatas['attributeValues'][$attr_code][0]->value_name, $matches)) {
            $wu_type_code = $matches[0];
        } else {
            // Значение атрибута не соответсвет ожидаемому
            return Null;
        }
        // Получим id номера на WuBook на основе значения атрибута
        $query = $this->_db->getQuery(true);
        $query->select($this->_db->quoteName('wu_id'));
        $query->from($this->_db->quoteName('#__discountcards_wurooms'));
        $query->where($this->_db->quoteName('shortname') . ' LIKE ' . $this->_db->quote('%' . $wu_type_code . '%'));
        $this->_db->setQuery($query);
        $result = $this->_db->loadAssoc();
        
        if (! $result) {
            return Null;
        }
        return $result['wu_id'];
    }

    /**
     * Параметры компонента
     *
     * @var Joomla\Registry\Registry
     */
    public $params;

    /**
     *
     * @param array $config            
     */
    public function __construct($config = array())
    {
        parent::__construct($config);
        $this->params = $config['params'];
        
        $url = $this->params->get('wu_server', '');
        $this->rpc_client = new xmlrpc_client($url);
        $this->wu_lcode = $this->params->get('wu_lcode', '');
    }

    /**
     * Запрашивает данные о ценах с сервера WuBook, вызывая функцию API fetch_plan_prices
     *
     * @link http://tdocs.wubook.net/wired/prices.html?highlight=fetch_plan_prices#fetch_plan_prices
     *      
     * @param int $price_id            
     * @param DateTime $dateFrom            
     * @param DateTime $dateTo            
     * @param array $rooms
     *            - массив из id номеров в сервисе WuBook
     * @return mixed[][]
     */
    public function fetch_plan_prices($price_id, DateTime $dateFrom, DateTime $dateTo, $rooms = array())
    {
        // Подготовим список комнат
        $rooms_rpc = array();
        foreach ($rooms as $room) {
            $wu_code = $this->getWuRoomId($room);
            
            if (! is_null($wu_code)) {
                $rooms_rpc[] = new xmlrpcval($wu_code, 'int');
            }
        }
        // Подготовим остальные параметры
        $wu_params = array(
            'pid' => (object) [
                'value' => $price_id,
                'type' => 'int'
            ],
            'dfrom' => (object) [
                'value' => $dateFrom->format('d/m/Y'),
                'type' => 'string'
            ],
            'dto' => (object) [
                'value' => $dateTo->format('d/m/Y'),
                'type' => 'string'
            ],
            'rooms' => (object) [
                'value' => $rooms_rpc,
                'type' => 'array'
            ]
        );
        // Выполняем запрос
        $result = $this->callWuFunc('fetch_plan_prices', $wu_params);
        // Освобождаем токен авторизации на WuBook
        $this->release_token();
        
        return $result;
    }

    /**
     * Запрашивает данные о тарифных планах с сервера WuBook, вызывая функцию API get_pricing_plans
     *
     * @link http://tdocs.wubook.net/wired/prices.html?highlight=get_pricing_plans#get_pricing_plans
     *      
     * @return mixed[][]
     */
    public function get_pricing_plans()
    {
        $result = $this->callWuFunc('get_pricing_plans');
        // Освобождаем токен авторизации на WuBook
        $this->release_token();
        
        return $result;
    }

    /**
     * Возвращает данные о категориях комнат с сервера WuBook, вызывая функцию API fetch_rooms
     *
     * @link http://tdocs.wubook.net/wired/rooms.html?highlight=fetch_rooms#fetch_rooms
     *      
     * @param boolean $ancillary
     *            - получить расширенный набор данных
     * @return mixed[][]
     */
    public function fetch_rooms($ancillary = true)
    {
        // Получаем данные от WuBook
        $rooms = $this->callWuFunc('fetch_rooms', array(
            'ancillary' => (object) [
                'value' => (int) $ancillary,
                'type' => 'int'
            ]
        ));
        // Освобождаем токен авторизации на WuBook
        $this->release_token();
        
        return $rooms;
    }

    /**
     * Добавляет в результат запроса размер скидки, если она предоставляется по карте
     *
     * @param mixed[][] $result
     *            Результат запроса к сервису WuBook
     * @param string $card_num
     *            Номер дисконтной карты
     * @return mixed[][] Результат с пересчитанными ценами
     */
    public function applyDiscount(&$result, $card_num)
    {
        // Получим размер скидки по номеру дисконтной карты
        $query = $this->_db->getQuery(true);
        $query->select($this->_db->quoteName('percentage'));
        $query->from($this->_db->quoteName('#__discountcards'));
        $query->where(array(
            $this->_db->quoteName('card_number') . ' = ' . $this->_db->quote($card_num),
            $this->_db->quoteName('state') . ' = ' . $this->_db->quote(1)
        ));
        $this->_db->setQuery($query);
        $db_res = $this->_db->loadAssoc();
        
        if (! $db_res) {
            return $result;
        }
        // Добавим в результат информацию о скидке 
        $percentage = $db_res['percentage'];
        
        foreach ($result[1] as $room_id => &$price) {
            $price['percentage'] = $percentage;
        }
        
        return $result;
    }
}
