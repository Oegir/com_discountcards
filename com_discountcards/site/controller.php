<?php
/**
 * @module		com_discountcards
 * @script		controller.php
 * @author      Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * General Controller of Ola component
 */
class DiscountcardsController extends JControllerLegacy
{

    /**
     * Параметры компонента
     *
     * @var Joomla\Registry\Registry
     */
    public $params;

    /**
     * Constructor.
     *
     * @param array $config
     *            An optional associative array of configuration settings.
     *            Recognized key values include 'name', 'default_task', 'model_path', and
     *            'view_path' (this list is not meant to be comprehensive).
     *            
     * @since 12.2
     */
    public function __construct($config = array())
    {
        parent::__construct($config);
        
        $this->params = JFactory::getApplication()->getParams();
    }

    /**
     * fetch_plan_prices task
     *
     * @return void
     */
    public function fetch_plan_prices()
    {
        $config = array(
            'params' => $this->params
        );
        $model = $this->getModel('wudata', 'discountcardsModel', $config);
        // Подготовим входные данные
        $app = JFactory::getApplication();
        $jinput = $app->input;
        try {
            // Выясним id прайса
            Switch ($jinput->get('paying', '', 'ALNUM')) {
                case 'prepay':
                    $price_id = $config['params']->get('wu_prepay_price_id', $pid);
                    break;
                
                case 'postpay':
                    $price_id = $config['params']->get('wu_postpay_price_id', $pid);
                    break;
                
                default:
                    // Прайс не распознан
                    throw new Exception(JText::_('COM_DISCOUNTCARDS_PAYING_TYPE_ERROR'));
                    return;
            }
            // Выясним промежуток дат
            $dfrom = new DateTime($jinput->get('dfrom', '', 'STRING'));
            $dto = new DateTime($jinput->get('dto', '', 'STRING'));
            // Выясним id номеров
            $rooms = $jinput->get('rooms', array(), 'ARRAY');
            // Получаем данные о ценах
            $result = $model->fetch_plan_prices($price_id, $dfrom, $dto, $rooms);
            // Применяем карту скидок
            $card_num = $jinput->get('card_num', '', 'ALNUM');
            if ((!empty($card_num)) && $result && isset($result[0]) && ($result[0] == 0)) {
                $result = $model->applyDiscount($result, $card_num);
            }
            
            echo new JResponseJson($result);
        } catch (Exception $e) {
            echo new JResponseJson($e);
        }
    }

    /**
     * get_pricing_plans task
     *
     * @return void
     */
    public function get_pricing_plans()
    {
        $app = JFactory::getApplication();
        $config = array(
            'params' => $app->getParams()
        );
        $model = $this->getModel('wudata', 'discountcardsModel', $config);
        
        try {
            $result = $model->get_pricing_plans($price_id, $dfrom, $dto, $rooms);
            
            echo new JResponseJson($result);
        } catch (Exception $e) {
            echo new JResponseJson($e);
        }
    }
}
