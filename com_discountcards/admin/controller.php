<?php
/**
 * @module		com_discountcards
 * @script		controller.php
 * @author      Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla controller library
jimport('joomla.application.component.controller');
 
/**
 * General Controller of Discountcards component
 */
class DiscountcardsController extends JControllerLegacy
{
	/**
	 * display task
	 *
	 * @return void
	 */
	function display($cachable = false)
	{	
		$view_name = JRequest::getCmd('view', 'discountcards');
	    JRequest::setVar('view', $view_name);
 
		// call parent behavior
		parent::display($cachable);
 
		// Set the submenu
		DiscountcardsHelper::addSubmenu($view_name);
	}
}
