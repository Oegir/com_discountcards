<?php
/**
 * @module		com_discountcards
 * @script		discountcards.php
 * @author      Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die();

/**
 * Discountcards component helper.
 */
abstract class DiscountcardsHelper
{

    /**
     * Configure the Linkbar.
     */
    public static function addSubmenu($submenu)
    {
        JSubMenuHelper::addEntry(JText::_('COM_DISCOUNTCARDS_SUBMENU_RECORDS'), 'index.php?option=com_discountcards', $submenu == 'discountcards');
        JSubMenuHelper::addEntry(JText::_('COM_DISCOUNTCARDS_SUBMENU_WUROOMS'), 'index.php?option=com_discountcards&view=wurooms', $submenu == 'wurooms');
        // set some global property
        $document = JFactory::getDocument();
        if ($submenu == 'categories') {
            $document->setTitle(JText::_('COM_DISCOUNTCARDS_ADMINISTRATION_CATEGORIES'));
        }
    }
    
    /**
     * Get the actions authorizations
     * 
     * @param number $messageId
     * @return JObject
     */
    public static function getActions($messageId = 0)
    {
        $user = JFactory::getUser();
        $result = new JObject();
        
        if (empty($messageId)) {
            $assetName = 'com_discountcards';
        } else {
            $assetName = 'com_discountcards.record.' . (int) $messageId;
        }
        
        $actions = array(
            'core.admin',
            'core.manage',
            'core.create',
            'core.edit',
            'core.delete'
        );
        
        foreach ($actions as $action) {
            $result->set($action, $user->authorise($action, $assetName));
        }
        
        return $result;
    }
}
