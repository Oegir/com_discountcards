<?php
/**
 * @module		com_discountcards
 * @author      Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

$archived  = $this->state->get('filter.state') == 2 ? true : false;
$trashed   = $this->state->get('filter.state') == -2 ? true : false;

foreach ($this->items as $i => $item) :
    $ordering = ($listOrder == 'ordering');
    $item->cat_link = JRoute::_('index.php?option=com_categories&extension=com_discountcards&task=edit&type=other&cid[]=' . $item->catid);
    ?>
<tr class="row<?php echo $i % 2; ?>">
    <td class="hidden-phone">
        <?php echo $item->id; ?>
    </td>
    <td>
		<?php echo JHtml::_('grid.id', $i, $item->id); ?>
	</td>
    <td class="center">
		<div class="btn-group">
			<?php echo JHtml::_('jgrid.published', $item->state, $i, 'discountcards.', true, 'cb', $item->publish_up, $item->publish_down); ?>
			<?php
			// Create dropdown items
			$action = $archived ? 'unarchive' : 'archive';
			JHtml::_('actionsdropdown.' . $action, 'cb' . $i, 'discountcards');

			$action = $trashed ? 'untrash' : 'trash';
			JHtml::_('actionsdropdown.' . $action, 'cb' . $i, 'discountcards');

			// Render dropdown list
			echo JHtml::_('actionsdropdown.render', $this->escape($item->name));
			?>
		</div>
	</td>
    <td class="small hidden-phone">
        <?php echo $item->card_number; ?>
    </td>
    <td class="nowrap has-context">
        <div class="pull-left">
            <?php if ($item->checked_out) : ?>
                <?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'discountcards.', true); ?>
            <?php endif; ?>
            <a href="<?php echo JRoute::_('index.php?option=com_discountcards&task=discountcard.edit&id=' . (int) $item->id); ?>">
                <?php echo $this->escape($item->name); ?></a>
        </div>
    </td>
    <td class="small hidden-phone">
        <?php echo sprintf('%01.2f', round($item->percentage, 2)) . '%'; ?>
    </td>
</tr>
<?php
endforeach;
?>
