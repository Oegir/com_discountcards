<?php
/**
 * @module		com_discountcards
 * @author      Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

$listOrder = $this->escape( preg_replace('/\W+\S+$/', '', $this->state->get('list.fullordering')));
$listDirn  = $this->escape($this->state->get('list.direction'));
?>
<tr>
    <th width="1%" class="nowrap hidden-phone">
        <?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
    </th>
    <th width="1%" class="center">
		<?php echo JHtml::_('grid.checkall'); ?>
	</th>
    <th width="1%" class="nowrap center">
        <?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.state', $listDirn, $listOrder); ?>
    </th>
    <th width="1%" class="nowrap center hidden-phone">
		<?php echo JHtml::_('searchtools.sort', 'COM_DISCOUNTCARDS_HEADING_CARTNUM', 'a.card_number', $listDirn, $listOrder); ?>
	</th>
    <th>
        <?php echo JHtml::_('searchtools.sort', 'COM_DISCOUNTCARDS_HEADING_NAME', 'a.name', $listDirn, $listOrder); ?>
    </th>
    <th width="1%" class="nowrap center hidden-phone">
        <?php echo JHtml::_('searchtools.sort', 'COM_DISCOUNTCARDS_HEADING_PERCENTAGE', 'a.percentage', $listDirn, $listOrder); ?>
    </th>
</tr>
