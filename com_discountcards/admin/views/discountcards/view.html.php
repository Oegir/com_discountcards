<?php
/**
 * @module		com_discountcards
 * @author-name Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * Viewer cписка скидочных карт
 */
class DiscountcardsViewDiscountcards extends JViewLegacy
{
    /**
     * Category data
     *
     * @var  array
     */
    protected $categories;
    
    /**
     * An array of items
     *
     * @var  array
     */
    protected $items;
    
    /**
     * The pagination object
     *
     * @var  JPagination
     */
    protected $pagination;
    
    /**
     * The model state
     *
     * @var  object
     */
    protected $state;
	/**
	 * Discountcards view display method
	 * @return void
	 */
	public function display($tpl = null) 
	{
	    $this->categories    = $this->get('CategoryOrders');
	    $this->items         = $this->get('Items');
	    $this->pagination    = $this->get('Pagination');
	    $this->state         = $this->get('State');
	    $this->filterForm    = $this->get('FilterForm');
	    $this->activeFilters = $this->get('ActiveFilters');
 
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
 
		// Set the toolbar
		$this->addToolBar();
 
		// Display the template
		parent::display($tpl);
 
		// Set the document
		$this->setDocument();
	}
 
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		$canDo = DiscountcardsHelper::getActions();
		JToolBarHelper::title(JText::_('COM_DISCOUNTCARDS_MANAGER_DISCOUNTCARDS'), 'Discountcards');
		if ($canDo->get('core.create')) 
		{
			JToolBarHelper::addNew('discountcard.add', 'JTOOLBAR_NEW');
		}
		if ($canDo->get('core.edit')) 
		{
			JToolBarHelper::editList('discountcard.edit', 'JTOOLBAR_EDIT');
		}
		if ($canDo->get('core.delete')) 
		{
			JToolBarHelper::deleteList('', 'discountcards.delete', 'JTOOLBAR_DELETE');
		}
		if ($canDo->get('core.admin')) 
		{
			JToolBarHelper::divider();
			JToolBarHelper::preferences('com_discountcards');
		}
	}
	
	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_DISCOUNTCARDS_ADMINISTRATION'));
	}
	
	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
	    return array(
	        'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
	        'a.state' => JText::_('JSTATUS'),
	        'a.card_number' => JText::_('COM_DISCOUNTCARDS_HEADING_CARTNUM'),
	        'a.name' => JText::_('COM_DISCOUNTCARDS_HEADING_NAME'),
	        'a.percentage' => JText::_('COM_DISCOUNTCARDS_HEADING_PERCENTAGE'),
	        'a.id' => JText::_('JGRID_HEADING_ID')
	    );
	}
}
