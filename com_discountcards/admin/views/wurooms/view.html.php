<?php
/**
 * @module		com_discountcards
 * @author-name Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * Discountcards View
 */
class DiscountcardsViewWurooms extends JViewLegacy
{
	/**
	 * Discountcards view display method
	 * @return void
	 */
	function display($tpl = null) 
	{
		// Get data from the model
		$items = $this->get('Items');
		$pagination = $this->get('Pagination');
 
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign data to the view
		$this->items = $items;
		$this->pagination = $pagination;
 
		// Set the toolbar
		$this->addToolBar();
 
		// Display the template
		parent::display($tpl);
 
		// Set the document
		$this->setDocument();
	}
 
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		$canDo = DiscountcardsHelper::getActions();
		JToolBarHelper::title(JText::_('COM_DISCOUNTCARDS_MANAGER_WUROOMS'), 'Discountcards.Wurooms');
		
		JToolBarHelper::custom( 'wurooms.update', 'refresh', '', JText::_('COM_DISCOUNTCARDS_WUROOMS_UPDATE'), false);
		
		if ($canDo->get('core.admin')) 
		{
			JToolBarHelper::divider();
			JToolBarHelper::preferences('com_discountcards');
		}
	}
	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_DISCOUNTCARDS_ADMINISTRATION'));
	}
}
