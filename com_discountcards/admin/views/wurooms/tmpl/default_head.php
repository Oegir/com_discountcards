<?php
/**
 * @module		com_discountcards
 * @author      Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<tr>
	<th width="1%" class="center">
		<?php echo JText::_('COM_DISCOUNTCARDS_CARDS_HEADING_ID'); ?>
	</th>
    <th width="20%" class="center">
        <?php echo JText::_('COM_DISCOUNTCARDS_CARDS_WUID'); ?>
    </th>
	<th width="49%" class="">
		<?php echo JText::_('COM_DISCOUNTCARDS_CARDS_WUNAME'); ?>
	</th>
    <th width="20%" class="center">
        <?php echo JText::_('COM_DISCOUNTCARDS_CARDS_WUSHNAME'); ?>
    </th>
    <th width="10%" class="center">
        <?php echo JText::_('COM_DISCOUNTCARDS_CARDS_WUPRICE'); ?>
    </th>
</tr>
