<?php
/**
 * @module		com_discountcards
 * @author      Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<tr>
	<td colspan="5"><?php echo $this->pagination->getListFooter(); ?></td>
</tr>
