<?php
/**
 * @module		com_discountcards
 * @author      Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php foreach($this->items as $i => $item): ?>
	<tr class="row<?php echo $i % 2; ?>">
		<td class="center">
			<?php echo $item->id; ?>
		</td>
		<td class="center">
			<?php echo $item->wu_id; ?>
		</td>
        <td class="">
            <?php echo $item->name; ?>
        </td>
        <td class="center">
            <?php echo $item->shortname; ?>
        </td>
        <td class="center">
            <?php echo $item->price; ?>
        </td>
	</tr>
<?php endforeach; ?>
