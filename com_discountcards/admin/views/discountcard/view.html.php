<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_discountcards
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View to edit a discountcard.
 *
 * @since  1.5
 */
class DiscountcardsViewDiscountcard extends JViewLegacy
{
	/**
	 * The JForm object
	 *
	 * @var  JForm
	 */
	protected $form;

	/**
	 * The active item
	 *
	 * @var  object
	 */
	protected $item;

	/**
	 * The model state
	 *
	 * @var  object
	 */
	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null)
	{
		// Initialiase variables.
		$this->form  = $this->get('Form');
		$this->item  = $this->get('Item');
		$this->state = $this->get('State');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));

			return false;
		}

		$this->addToolbar();
		JHtml::_('jquery.framework');

		return parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user       = JFactory::getUser();
		$userId     = $user->id;
		$isNew      = ($this->item->id == 0);
		$checkedOut = !($this->item->checked_out == 0 || $this->item->checked_out == $userId);

		JToolbarHelper::title($isNew ? JText::_('COM_DISCOUNTCARDS_MANAGER_BANNER_NEW') : JText::_('COM_DISCOUNTCARDS_MANAGER_BANNER_EDIT'), 'bookmark discountcards');

		// If not checked out, can save the item.
		if (!$checkedOut)
		{
			JToolbarHelper::apply('discountcard.apply');
			JToolbarHelper::save('discountcard.save');
		}

		if (empty($this->item->id))
		{
			JToolbarHelper::cancel('discountcard.cancel');
		}
		else
		{
			if ($this->state->params->get('save_history', 0) && $user->authorise('core.edit'))
			{
				JToolbarHelper::versions('com_discountcards.discountcard', $this->item->id);
			}

			JToolbarHelper::cancel('discountcard.cancel', 'JTOOLBAR_CLOSE');
		}
	}
}
