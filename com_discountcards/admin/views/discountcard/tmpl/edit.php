<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_discountcards
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');
?>

<form action="<?php echo JRoute::_('index.php?option=com_discountcards&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate">
	
	<div class="form-horizontal">
		<div class="row-fluid">
			<div class="span9">
				<?php 
				echo $this->form->getControlGroup('card_number') . PHP_EOL;
				echo $this->form->getControlGroup('name') . PHP_EOL;
				echo $this->form->getControlGroup('percentage') . PHP_EOL;
				?>
			</div>
			<div class="span3">
				<?php echo JLayoutHelper::render('joomla.edit.global', $this); ?>
			</div>
		</div>
	</div>

	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
