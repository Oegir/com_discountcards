<?php
/**
 * @module		com_discountcards
 * @script		discountcards.php
 * @author      Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');
JLoader::register('discountcardsModelWudata', JPATH_SITE . '/components/com_discountcards/models/wudata.php');

/**
 * DiscountcardsList Model
 */
class DiscountcardsModelWurooms extends JModelList
{

    /**
     * Method to build an SQL query to load the list data.
     *
     * @return string An SQL query
     */
    protected function getListQuery()
    {
        // Create a new query object.
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        // Select some fields
        $query->select($db->quoteName(array(
            'id',
            'catid',
            'wu_id',
            'name',
            'shortname',
            'occupancy',
            'men',
            'children',
            'subroom',
            'anchorate',
            'price',
            'availability',
            'board',
            'boards',
            'woodoo',
            'dec_avail',
            'min_price',
            'max_price',
            'params'
        )));
        // From the hello table
        $query->from($db->quoteName('#__discountcards_wurooms'));
        return $query;
    }

    /**
     * Обновляет данные о категориях номеров с сервиса WuBook.net через Wired Api
     *
     * @param Joomla\Registry\Registry $params            
     */
    public function wu_update($params)
    {
        // Получим настройки компонента
        $server = $params->get('wu_server', '');
        $user = $params->get('wu_user', '');
        $pass = $params->get('wu_pass', '');
        $pkey = $params->get('wu_pkey', '');
        $lcode = $params->get('wu_lcode', '');
        // Проверим корректность настроек
        if (empty($server) or empty($user) or empty($pass) or empty($pkey) or empty($lcode)) {
            JError::raiseError('', JText::_('COM_DISCOUNTCARDS_WUROOMS_CREDENTIALS_NOT_SETTED'));
            return;
        }
        // Получим данные о категориях номеров
        $config = array(
            'params' => $params
        );
        $wu_model = new discountcardsModelWudata($config);
        $wu_rooms = $wu_model->fetch_rooms();
        // Проверим, что не возникло ошибок при запросе
        if ((! isset($wu_rooms[0])) or $wu_rooms[0] != 0 or (! isset($wu_rooms[1]))) {
            $error_msg = JText::_('COM_DISCOUNTCARDS_WUROOMS_FETCHING_ROOMS_ERROR');
            
            if (isset($wu_rooms[1])) {
                $error_msg .= '<br />' . JText::_('COM_DISCOUNTCARDS_SERVER_SAYS') . ' ' . $wu_rooms[1];
            }
            JError::raiseError('', $error_msg);
            return;
        }
        // Очищаем прежние данные
        $db = $this->getDbo();
        $db->truncateTable('#__discountcards_wurooms');
        // Подготавливаем запрос для сохранения данных
        foreach ($wu_rooms[1] as $room) {
            // Проверим наличие NOT NULL полей
            if ((! isset($room['id'])) or (! isset($room['name'])) or (! isset($room['shortname']))) {
                JError::raiseWarning('', JText::_('COM_DISCOUNTCARDS_SOME_ROOM_ERROR'));
                continue;
            }
            // Добавим NOT NULL поля
            $columns = array(
                'wu_id',
                'name',
                'shortname'
            );
            $values = array(
                $room['id'],
                '"' . $room['name'] . '"',
                '"' . $room['shortname'] . '"'
            );
            // Добавим остальные поля при наличии
            if (isset($room['occupancy'])) {
                $columns[] = 'occupancy';
                $values[] = $room['occupancy'];
            }
            if (isset($room['men'])) {
                $columns[] = 'men';
                $values[] = $room['men'];
            }
            if (isset($room['children'])) {
                $columns[] = 'children';
                $values[] = $room['children'];
            }
            if (isset($room['subroom'])) {
                $columns[] = 'subroom';
                $values[] = $room['subroom'];
            }
            if (isset($room['anchorate'])) {
                $columns[] = 'anchorate';
                $values[] = $room['anchorate'];
            }
            if (isset($room['price'])) {
                $columns[] = 'price';
                $values[] = $room['price'];
            }
            if (isset($room['availability'])) {
                $columns[] = 'availability';
                $values[] = $room['availability'];
            }
            if (isset($room['board'])) {
                $columns[] = 'board';
                $values[] = '"' . $room['board'] . '"';
            }
            if (isset($room['boards'])) {
                $columns[] = 'boards';
                $values[] = '"' . $room['boards'] . '"';
            }
            if (isset($room['woodoo'])) {
                $columns[] = 'woodoo';
                $values[] = $room['woodoo'];
            }
            if (isset($room['dec_avail'])) {
                $columns[] = 'dec_avail';
                $values[] = $room['dec_avail'];
            }
            if (isset($room['min_price'])) {
                $columns[] = 'min_price';
                $values[] = $room['min_price'];
            }
            if (isset($room['max_price'])) {
                $columns[] = 'max_price';
                $values[] = $room['max_price'];
            }
            // Соберем и выполним запрос
            $query = $db->getQuery(true);
            $query->insert($db->quoteName('#__discountcards_wurooms'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $db->execute();
        }
    }
}
