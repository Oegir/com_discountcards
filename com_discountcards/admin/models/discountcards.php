<?php
/**
 * @module		com_discountcards
 * @script		discountcards.php
 * @author      Alexey Petrov
 * @copyright	Copyright © 2016 T.T.Consalting. All rights reserved.
 * @license		GNU/GPL, see http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * DiscountcardsList Model
 */
class DiscountcardsModelDiscountcards extends JModelList
{
    /**
     * Method to build an SQL query to load the list data.
     *
     * @return string An SQL query
     */
    protected function getListQuery()
    {
        // Create a new query object.
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        // Select some fields
        $query->select(
            $db->quoteName('a.id', 'id') . ',' .
			$db->quoteName('a.name', 'name') . ',' .
			$db->quoteName('a.card_number', 'card_number') . ',' .
            $db->quoteName('a.percentage', 'percentage') . ',' .
			$db->quoteName('a.checked_out', 'checked_out') . ',' .
			$db->quoteName('a.checked_out_time', 'checked_out_time') . ',' .
			$db->quoteName('a.catid', 'catid') . ',' .
			$db->quoteName('a.state', 'state') . ',' .
			$db->quoteName('a.ordering', 'ordering') . ',' .
			$db->quoteName('a.language') . ',' .
			$db->quoteName('a.publish_up') . ',' .
			$db->quoteName('a.publish_down')
            );
        // From the hello table
        $query->from($db->quoteName('#__discountcards', 'a'));
        // Filter by published state
        $published = $this->getState('filter.published');
        
        if (is_numeric($published))
        {
            $query->where('a.state = ' . (int) $published);
        }
        elseif ($published === '')
        {
            $query->where('(a.state IN (0, 1))');
        }
        
        // Filter by search in title
        $search = $this->getState('filter.search');
        
        if (!empty($search))
        {
            if (stripos($search, 'id:') === 0)
            {
                $query->where('a.id = ' . (int) substr($search, 3));
            }
            else
            {
                $search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
                $query->where('(a.name LIKE ' . $search . ' OR a.card_number LIKE ' . $search . ')');
            }
        }
            // Add the list ordering clause.
        $fullordering = $this->state->get('list.fullordering', 'ordering ASC');
        
        if (! empty($fullordering)) {
            $query->order($db->escape($fullordering . ' ' . $orderDirn));
        }
        
        return $query;
    }
}
