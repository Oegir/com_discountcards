<?php
/**
 * Pricing plans List Field class for Discountcards.
 *
 * @package     Joomla
 * @subpackage  Form
 * @copyright   Copyright (C) 2005-2013 fabrikar.com - All rights reserved.
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');
JLoader::register('discountcardsModelWudata', JPATH_SITE . '/components/com_discountcards/models/wudata.php');

/**
 * Plugin List Field class for Fabrik.
 *
 * @package Joomla
 * @subpackage Form
 * @since 1.6
 */
class JFormFieldPricingPlans extends JFormFieldList
{

    /**
     * The form field type.
     *
     * @var string
     * @since 1.6
     */
    protected $type = 'PricingPlans';

    /**
     * Method to get the field options.
     *
     * @return array The field option objects.
     */
    protected function getOptions()
    {
        // Проверим, что заданы параметры доступа к WuBook
        $params = JComponentHelper::getParams('com_discountcards');
        
        $server = $params->get('wu_server', '');
        $user = $params->get('wu_user', '');
        $pass = $params->get('wu_pass', '');
        $pkey = $params->get('wu_pkey', '');
        $lcode = $params->get('wu_lcode', '');
        
        if (empty($server) or empty($user) or empty($pass) or empty($pkey) or empty($lcode)) {
            $options = array(
                JHtml::_('select.option', '', FText::_('COM_DISCOUNTCARDS_PLEASE_DEFINE_CREDENTIALS'))
            );
            return $options;
        }
        // Получим данные о прайсах
        $config = array(
            'params' => $params
        );
        $model = new discountcardsModelWudata($config);
        $pricing_plans = $model->get_pricing_plans();
        // Проверим, что не возникло ошибок при запросе
        if ((! isset($pricing_plans[0])) or $pricing_plans[0] != 0) {
            $options = array(
                JHtml::_('select.option', '', FText::_('COM_DISCOUNTCARDS_ERROR_GETTING_PRICING_PLANS'))
            );
            return $options;
        }
        $options = array();
        // Сформируем данные для списка выбора
        foreach ($pricing_plans[1] as $price) {
            $options[] = (object) [
                'text' => $price['id'] . ': ' . $price['name'],
                'value' => $price['id']
            ];
            $a = 0;
        }
        array_unshift($options, JHtml::_('select.option', '', FText::_('COM_DISCOUNTCARDS_PLEASE_SELECT')));
        
        return $options;
    }
}
