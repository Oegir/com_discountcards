DROP TABLE IF EXISTS `#__discountcards`;
 
CREATE TABLE `#__discountcards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `card_number` varchar(45) UNIQUE NOT NULL,
  `percentage` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `language` char(7) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_language` (`language`),
  KEY `idx_card_number` (`language`)
) AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__discountcards_wurooms`;

CREATE TABLE `#__discountcards_wurooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `wu_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `shortname` varchar(45) NOT NULL,
  `occupancy` int(11) DEFAULT '0',
  `men` int(11) DEFAULT '0',
  `children` int(11) DEFAULT '0',
  `subroom` int(11) DEFAULT '0',
  `anchorate` int(11) DEFAULT '0',
  `price` decimal(16,6) DEFAULT NULL,
  `availability` varchar(45) DEFAULT NULL,
  `board` varchar(45) DEFAULT NULL,
  `boards` varchar(45) DEFAULT NULL,
  `woodoo` varchar(45) DEFAULT NULL,
  `dec_avail` varchar(45) DEFAULT NULL,
  `min_price` decimal(16,6) DEFAULT NULL,
  `max_price` decimal(16,6) DEFAULT NULL,
  `params` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wu_id_UNIQUE` (`wu_id`),
  UNIQUE KEY `shortname_UNIQUE` (`shortname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

